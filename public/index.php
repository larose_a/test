<?php

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();

$app->before(function (Request $request, Silex\Application $app) {
    
});

$app->get('/hello/{name}', function ($name) use ($app) {
    return 'Hello '.$app->escape($name);
});

$app->get('/haha', function () use ($app) {
    return 'haha ';
});


$app->after(function (Request $request, Response $response) {
    $response->setContent("test");
});


$app->run();